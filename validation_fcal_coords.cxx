//This code snippet can be added to the jFEXFPGA.cxx file, inside the execute function
//It will print the TriggerTower and respective global eta phi coordinates for a Jet constructed
//around the central trigger tower. Then in the following two below, indicated with the "-----"
//couts, the trigger towers which fall into the seed, first energy ring and second energy ring will have their
//TT ID printed along with central eta phi coordinate. 


  if(m_jfexid ==0 && m_id ==1){ //Indicate jfexid (only 0 and 5) and fpga id. 
  std::map<int, jFEXForwardJetsInfo> FCALJets =  m_jFEXForwardJetsAlgoTool->calculateJetETs();
    for(std::map<int, jFEXForwardJetsInfo>::iterator it = FCALJets.begin(); it!=(FCALJets.end()); ++it)
    {
       int TriggerTower = it->first;
       jFEXForwardJetsInfo FCALJetsInfo = it->second;
       std::cout<<"---------- Central Trigger Tower Information ---------- "<< std::endl;
       printf("%10d %12f %12f \n",TriggerTower, FCALJetsInfo.getCentreTTEta(),FCALJetsInfo.getCentreTTPhi() );
 
       std::cout<<"---------- Seed Trigger Tower Information ----------"<<std::endl;
       for (std::size_t i = 0, max = FCALJetsInfo.getTTinSeed().size(); i != max; ++i){
         SG::ReadHandle<jTowerContainer> jk_jFEXForwardJetsAlgo_jTowerContainer(m_jFEXFPGA_jTowerContainerKey/*,ctx*/);
         const LVL1::jTower * tmpTower = jk_jFEXForwardJetsAlgo_jTowerContainer->findTower(FCALJetsInfo.getTTinSeed()[i]);
         float centreEta = tmpTower->centreEta();
         float centrePhi = tmpTower->centrePhi();
         printf("%10d %12f %12f \n", FCALJetsInfo.getTTinSeed()[i], centreEta, centrePhi);
       } 
 
       std::cout<<"---------- First energy Ring Trigger Tower Information ----------"<<std::endl;
       for (std::size_t i = 0, max = FCALJetsInfo.getTTIDinFirstER().size(); i != max; ++i){
         SG::ReadHandle<jTowerContainer> jk_jFEXForwardJetsAlgo_jTowerContainer(m_jFEXFPGA_jTowerContainerKey/*,ctx*/);
         const LVL1::jTower * tmpTower = jk_jFEXForwardJetsAlgo_jTowerContainer->findTower(FCALJetsInfo.getTTIDinFirstER()[i]);
         float centreEta = tmpTower->centreEta();
         float centrePhi = tmpTower->centrePhi();
         printf("%10d %12f %12f \n", FCALJetsInfo.getTTIDinFirstER()[i], centreEta, centrePhi);
       }
 
       std::cout<<"---------- Second energy Ring Trigger Tower Information ----------"<<std::endl;
       for (std::size_t i = 0, max = FCALJetsInfo.getTTIDinSecondER().size(); i != max; ++i){
         SG::ReadHandle<jTowerContainer> jk_jFEXForwardJetsAlgo_jTowerContainer(m_jFEXFPGA_jTowerContainerKey/*,ctx*/);
         const LVL1::jTower * tmpTower = jk_jFEXForwardJetsAlgo_jTowerContainer->findTower(FCALJetsInfo.getTTIDinSecondER()[i]);
         float centreEta = tmpTower->centreEta();
         float centrePhi = tmpTower->centrePhi();
         printf("%10d %12f %12f \n", FCALJetsInfo.getTTIDinSecondER()[i], centreEta, centrePhi);
         }      
       std::cout<<"-------------------------------------------------------------------"<<std::endl;        

       //This line below will allow one to see more information on
       //std::cout<<TriggerTower<<" | "<<FCALJetsInfo.getCentreTTPhi()<< " | "<<FCALJetsInfo.getCentreTTEta()<<" | "<<FCALJetsInfo.getSeedET()<<" | "<< FCALJetsInfo.getFirstEnergyRingET()<<" | "<<FCALJetsInfo.getSecondEnergyRingET()<<std::endl; 
  }
}

//This code snippet can also be placed in jFEXFPGA.execute() to print local eta phi coordinates and their respective global eta phi coordinates
//This is useful to save in a data file for mapping validation plots.
if(m_jfexid == 0 && m_id==1){  //indicate jfexid (only 0 and 5) and fpga id
  for(int centre_nphi = 0; centre_nphi < FEXAlgoSpaceDefs::jFEX_algoSpace_height; centre_nphi++) {
    for(int centre_neta = 0; centre_neta < FEXAlgoSpaceDefs::jFEX_wide_algoSpace_width; centre_neta++) {
      if(m_jTowersIDs_Wide[centre_nphi][centre_neta]!=0){
         printf("%7d %7d %12f %12f \n",centre_nphi, centre_neta , m_jFEXForwardJetsAlgoTool->globalPhi(centre_nphi, centre_neta), m_jFEXForwardJetsAlgoTool->globalEta(centre_nphi, centre_neta));}
   }
  }
}


